package com.service.personservice.infraestructura.persistence.dao;

import com.service.personservice.infraestructura.persistence.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonDao extends JpaRepository<PersonEntity,Long> {
}
