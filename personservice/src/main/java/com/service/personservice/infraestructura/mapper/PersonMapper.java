package com.service.personservice.infraestructura.mapper;

import com.service.personservice.domain.model.Person;
import com.service.personservice.infraestructura.persistence.entity.PersonEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonMapper {
    PersonEntity toPersonEntity (Person person);
    List<PersonEntity> toPersonEntity (List<Person> person);
    @InheritInverseConfiguration
    Person toPerson(PersonEntity personEntity);
    List<Person> toPersonList(List<PersonEntity> personEntityList);


}
