package com.service.personservice.infraestructura.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "persona")
public class PersonEntity {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long personId;
    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;
    int age;

}
