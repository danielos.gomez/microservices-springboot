package com.service.personservice.infraestructura.persistence.repository;

import com.service.personservice.domain.model.Person;
import com.service.personservice.domain.ports.PersonRepository;
import com.service.personservice.infraestructura.mapper.PersonMapper;
import com.service.personservice.infraestructura.persistence.dao.PersonDao;
import com.service.personservice.infraestructura.persistence.entity.PersonEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {


    private  final PersonDao personDao;
    private  final PersonMapper personMapper;

    public PersonRepositoryImpl(PersonDao personDao, PersonMapper personMapper) {
        this.personDao = personDao;
        this.personMapper = personMapper;
    }

    @Transactional
    @Override
    public Person savePerson(Person person) {
        PersonEntity personEntity = personMapper.toPersonEntity(person);
        return personMapper.toPerson(personDao.save(personEntity));
    }

    @Override
    public void deletePerson(Person person) {
        personDao.delete(personMapper.toPersonEntity(person));

    }

    @Override
    public Person getPerson(Long id) {
        return personMapper.toPerson(personDao.getById(id));
    }

    @Override
    public List<Person> getAllPersons() {
        return personMapper.toPersonList(personDao.findAll());
    }
}
