package com.service.personservice.infraestructura.controllers;

import com.service.personservice.aplicacion.dto.PersonWithPhotoDto;
import com.service.personservice.aplicacion.usescases.PersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    @Autowired
    PersonUseCase personUseCase;

    @PostMapping()
    public ResponseEntity
    savePersonWithPhoto(@RequestBody PersonWithPhotoDto personWithPhotoDto){
       try{
        personUseCase.savePerson(personWithPhotoDto);
        return new ResponseEntity(HttpStatus.OK);
    }
       catch (Exception e){
           return  new ResponseEntity(HttpStatus.BAD_REQUEST);
       }
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonWithPhotoDto> getPersonById ( @PathVariable("id") Long id){
     //   try{
            return new ResponseEntity<PersonWithPhotoDto>(personUseCase.getPerson(id),HttpStatus.OK);
    //    }catch (Exception e){
    //        return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    //    }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePersonById ( @PathVariable("id") Long id){
        try{
            personUseCase.deletePerson(id);
            return new ResponseEntity(HttpStatus.OK);
        }catch (Exception e){
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}
