package com.service.personservice.domain.serviceimpl;

import com.service.personservice.aplicacion.builders.BuilderDto;
import com.service.personservice.aplicacion.feignclients.PhotoFeignClient;
import com.service.personservice.domain.services.ServicePhotoService;

import java.util.List;

public class ServicePhotoFeignServiceImpl  implements ServicePhotoService {

    PhotoFeignClient photoFeignClient;

    public ServicePhotoFeignServiceImpl(PhotoFeignClient photoFeignClient) {
        this.photoFeignClient = photoFeignClient;
    }

    @Override
    public List<String> getPhotoListByPersonId(Long personId) {
      return photoFeignClient.getPhotosByPersonId(personId).getBody();
    }

    @Override
    public void savePhotosByPersonId(List<String> photoList, Long personId) {
        BuilderDto builderDto = new BuilderDto();
        photoFeignClient.insertPhotos(builderDto.buildPhotoDto(personId,photoList));

    }

    @Override
    public void deletePhotosByPersonId(Long id) {
        photoFeignClient.deletePhotosByPersonId(id);

    }
}
