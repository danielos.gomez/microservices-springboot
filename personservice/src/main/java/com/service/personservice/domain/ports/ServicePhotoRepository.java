package com.service.personservice.domain.ports;

import com.service.personservice.domain.model.Photo;

import java.util.List;

public interface ServicePhotoRepository {
    List<String> getPhotoListByPersonId(Long personId);
    void savePhoto(Photo photo);
}
