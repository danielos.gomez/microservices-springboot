package com.service.personservice.domain.configuration;

import com.service.personservice.aplicacion.feignclients.PhotoFeignClient;
import com.service.personservice.domain.ports.PersonRepository;
import com.service.personservice.domain.serviceimpl.PersonServiceImpl;
import com.service.personservice.domain.serviceimpl.ServicePhotoFeignServiceImpl;
import com.service.personservice.domain.services.PersonService;
import com.service.personservice.domain.services.ServicePhotoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBeansDomain {

    /*
    @Bean
    public ServicePhotoService servicePhotoServiceBean (RestTemplate restTemplate){
        return new ServicePhotoServiceImpl(restTemplate);
    }

     */

    @Bean
    public ServicePhotoService servicePhotoServiceFeingClientBean(PhotoFeignClient photoFeignClient){
        return new ServicePhotoFeignServiceImpl(photoFeignClient);
    }

    @Bean
    public PersonService ServicePersona(PersonRepository personRepository) {
        return new PersonServiceImpl(personRepository);
    }
}
