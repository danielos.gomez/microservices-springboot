package com.service.personservice.domain.model;

import java.util.List;

public class Photo {
    Long personId;
    List<String> photoList;

    public Photo() {
    }

    public Photo(Long personId, List<String> photoList) {
        this.personId = personId;
        this.photoList = photoList;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public List<String> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<String> photoList) {
        this.photoList = photoList;
    }
}
