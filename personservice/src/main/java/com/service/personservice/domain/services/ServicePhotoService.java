package com.service.personservice.domain.services;

import java.util.List;

public interface ServicePhotoService {
    List<String> getPhotoListByPersonId(Long personId);
    void savePhotosByPersonId(List<String> photoList, Long personId);
    void deletePhotosByPersonId(Long id);
}
