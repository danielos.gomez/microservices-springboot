package com.service.personservice.domain.serviceimpl;

import com.service.personservice.domain.model.Photo;
import com.service.personservice.domain.services.ServicePhotoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;


public class ServicePhotoServiceImpl implements ServicePhotoService {

RestTemplate restTemplate;

    public ServicePhotoServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Override
    public List<String> getPhotoListByPersonId(Long id) {
       List<String> photoList =  restTemplate.getForObject("http://localhost:8002/photos/"+id ,List.class);
       return photoList;
    }

    @Override
    public void savePhotosByPersonId(List<String> photoList, Long personId) {
        Photo photo = new Photo();
        photo.setPhotoList(photoList);
        photo.setPersonId(personId);
        restTemplate.postForObject("http://localhost:8002/photos", photo , ResponseEntity.class);


    }

    @Override
    public void deletePhotosByPersonId(Long id) {
        restTemplate.delete("http://localhost:8002/photos/"+id);
    }


}
