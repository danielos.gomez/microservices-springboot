package com.service.personservice.domain.services;

import com.service.personservice.domain.model.Person;

import java.util.List;

public interface PersonService {
    Person savePerson(Person person);
    List<Person> getAllPersons();
    Person getPersonById(Long id);
    void deletePerson(Long id);
}
