package com.service.personservice.domain.serviceimpl;

import com.service.personservice.domain.ports.PersonRepository;
import com.service.personservice.domain.services.PersonService;
import com.service.personservice.domain.model.Person;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    @Override
    public Person savePerson(Person person) {
        return personRepository.savePerson(person);
    }

    @Override
    public List<Person> getAllPersons() {
       return personRepository.getAllPersons();
    }

    @Override
    public Person getPersonById(Long id) {
        return personRepository.getPerson(id);
    }

    @Override
    public void deletePerson(Long id) {
        personRepository.deletePerson(getPersonById(id));

    }
}
