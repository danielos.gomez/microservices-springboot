package com.service.personservice.domain.ports;

import com.service.personservice.domain.model.Person;

import java.util.List;

public interface PersonRepository {

    Person savePerson(Person person);
    void deletePerson(Person person);
    Person getPerson(Long id);
    List<Person> getAllPersons();

}
