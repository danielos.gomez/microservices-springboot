package com.service.personservice.aplicacion.mapper;

import com.service.personservice.aplicacion.dto.PersonDto;
import com.service.personservice.domain.model.Person;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface PersonDtoMapper {
    Person toPerson (PersonDto personDto);
    List<Person> toPersonList (List<Person> personList);

    @InheritInverseConfiguration

    PersonDto toPersonDto (Person person);
    List<PersonDto> toPersonDtoList (List<Person> personList);



}
