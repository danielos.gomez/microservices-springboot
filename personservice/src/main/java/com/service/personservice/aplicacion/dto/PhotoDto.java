package com.service.personservice.aplicacion.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PhotoDto {
    Long personId;
    List<String> photoList;
}
