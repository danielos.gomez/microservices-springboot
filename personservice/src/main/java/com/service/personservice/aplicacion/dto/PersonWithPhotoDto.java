package com.service.personservice.aplicacion.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonWithPhotoDto {
    Long personId;
    String firstName;
    String lastName;
    int age;
    List<String> photoList;


}
