package com.service.personservice.aplicacion.builders;

import com.service.personservice.aplicacion.dto.PersonWithPhotoDto;
import com.service.personservice.aplicacion.dto.PhotoDto;
import com.service.personservice.domain.model.Person;

import java.util.List;

public class BuilderDto {


 public  PersonWithPhotoDto buildPersonWithPhotoDto(Person person , List<String> listPhoto){
  return PersonWithPhotoDto.builder()
           .personId(person.getPersonId())
           .firstName(person.getFirstName())
           .lastName(person.getLastName())
           .age(person.getAge())
           .photoList(listPhoto).build();

 }

 public PhotoDto  buildPhotoDto(Long personId, List<String> photoList){
     return PhotoDto.builder().
             photoList(photoList).
             personId(personId).build();

 }

}
