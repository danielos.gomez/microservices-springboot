package com.service.personservice.aplicacion.configuration;

import com.service.personservice.aplicacion.mapper.PersonDtoMapper;
import com.service.personservice.aplicacion.usecasesImpl.PersonUseCaseImpl;
import com.service.personservice.aplicacion.usescases.PersonUseCase;
import com.service.personservice.domain.services.PersonService;
import com.service.personservice.domain.services.ServicePhotoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBeansApplication {

    @Bean
    public PersonUseCase caseUsePerson(PersonService personService, ServicePhotoService servicePhotoService,
                                       PersonDtoMapper personDtoMapper) {
        return new PersonUseCaseImpl(personService,servicePhotoService,personDtoMapper);
    }



}
