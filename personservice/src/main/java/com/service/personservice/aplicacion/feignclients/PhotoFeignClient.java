package com.service.personservice.aplicacion.feignclients;

import com.service.personservice.aplicacion.dto.PhotoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "photo-service",url = "http://localhost:8002" , path = "/photos")
public interface PhotoFeignClient {
    @PostMapping
    ResponseEntity insertPhotos(@RequestBody PhotoDto photoDto);
    @GetMapping("/{personId}")
    ResponseEntity<List<String>> getPhotosByPersonId(@PathVariable("personId") Long personId );
    @DeleteMapping("/{personId}")
    ResponseEntity deletePhotosByPersonId(@PathVariable("personId") Long personId );
}
