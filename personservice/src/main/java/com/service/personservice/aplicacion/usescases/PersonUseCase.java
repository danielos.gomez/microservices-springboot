package com.service.personservice.aplicacion.usescases;

import com.service.personservice.aplicacion.dto.PersonWithPhotoDto;

import java.util.List;

public interface PersonUseCase {
    void savePerson(PersonWithPhotoDto person);
    PersonWithPhotoDto getPerson(Long id);
    List<PersonWithPhotoDto> getAllPersons();
    void deletePerson(Long id);

}
