package com.service.personservice.aplicacion.usecasesImpl;

import com.service.personservice.aplicacion.builders.BuilderDto;
import com.service.personservice.aplicacion.dto.PersonDto;
import com.service.personservice.aplicacion.dto.PersonWithPhotoDto;
import com.service.personservice.aplicacion.mapper.PersonDtoMapper;
import com.service.personservice.aplicacion.usescases.PersonUseCase;
import com.service.personservice.domain.model.Person;
import com.service.personservice.domain.services.PersonService;
import com.service.personservice.domain.services.ServicePhotoService;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class PersonUseCaseImpl implements PersonUseCase {

   PersonService personService;
   ServicePhotoService servicePhotoService;
   PersonDtoMapper personDtoMapper;

    public PersonUseCaseImpl(PersonService personService, ServicePhotoService servicePhotoService,
                             PersonDtoMapper personDtoMapper) {
        this.personService = personService;
        this.servicePhotoService = servicePhotoService;
        this.personDtoMapper = personDtoMapper;
    }

    @Override
    @Transactional
    public void savePerson(PersonWithPhotoDto personWithPhotoDto) {

        //Mysql
        PersonDto personDto = new PersonDto();
        BeanUtils.copyProperties(personWithPhotoDto,personDto);
        Long personID = personService.savePerson(personDtoMapper.toPerson(personDto)).getPersonId();

        //Mongo
        List<String> photoList = personWithPhotoDto.getPhotoList();
        servicePhotoService.savePhotosByPersonId(photoList, personID);

    }

    @Override
    public PersonWithPhotoDto getPerson(Long id) {
        BuilderDto builder = new BuilderDto();
        List<String> photoList = servicePhotoService.getPhotoListByPersonId(id);
        Person person = personService.getPersonById(id);
        PersonWithPhotoDto personWithPhotoDto;
        personWithPhotoDto=builder.buildPersonWithPhotoDto(person,photoList);
        return  personWithPhotoDto;

    }

    @Override
    public List<PersonWithPhotoDto> getAllPersons() {
        return null;
    }

    @Override
    public void deletePerson(Long id) {
        personService.deletePerson(id);
        servicePhotoService.deletePhotosByPersonId(id);
    }
}
