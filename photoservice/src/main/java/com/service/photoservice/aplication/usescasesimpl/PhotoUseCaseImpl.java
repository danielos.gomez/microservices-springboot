package com.service.photoservice.aplication.usescasesimpl;

import com.service.photoservice.aplication.dto.PhotoDto;
import com.service.photoservice.aplication.mapper.PhotoDtoMapper;
import com.service.photoservice.aplication.usescases.PhotoUseCase;
import com.service.photoservice.domain.services.PhotoService;
import org.springframework.stereotype.Service;

import java.util.List;


public class PhotoUseCaseImpl implements PhotoUseCase {

    PhotoService photoService;
    PhotoDtoMapper photoDtoMapper;

    public PhotoUseCaseImpl(PhotoService photoService, PhotoDtoMapper photoDtoMapper) {
        this.photoService = photoService;
        this.photoDtoMapper = photoDtoMapper;
    }

    public PhotoUseCaseImpl() {
    }

    @Override
    public void savePhotos(PhotoDto photoDto) {
        photoService.savePhoto(photoDtoMapper.toPhoto(photoDto));
    }

    @Override
    public List<String> getPhotos(Long personId) {
        return photoService.getPhotoListByPersonId(personId);
    }

    @Override
    public void deletePhotos(Long personId) {
         photoService.deletePhotos(personId);
    }


}
