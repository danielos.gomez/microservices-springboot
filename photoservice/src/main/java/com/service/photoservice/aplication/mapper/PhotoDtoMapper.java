package com.service.photoservice.aplication.mapper;

import com.service.photoservice.aplication.dto.PhotoDto;
import com.service.photoservice.domain.model.Photo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PhotoDtoMapper {

    @Mappings({
            @Mapping(source = "photoList", target = "photoList"),
            @Mapping(source = "personId", target = "personId")
    })
    PhotoDto toPhotoDto (Photo photo);
    List<PhotoDto> toPhotoDtoList (List<Photo> photoList);

    @InheritInverseConfiguration

    @Mapping(target = "photoId", ignore = true)
    Photo toPhoto (PhotoDto photoDto);
    List<Photo> toPhotoList (List<PhotoDto> photoDtoList);
}
