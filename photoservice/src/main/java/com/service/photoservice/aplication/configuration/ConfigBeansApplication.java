package com.service.photoservice.aplication.configuration;

import com.service.photoservice.aplication.mapper.PhotoDtoMapper;
import com.service.photoservice.aplication.usescases.PhotoUseCase;
import com.service.photoservice.aplication.usescasesimpl.PhotoUseCaseImpl;
import com.service.photoservice.domain.services.PhotoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBeansApplication {
    @Bean
    public PhotoUseCase photoUseCaseBean(PhotoService photoService, PhotoDtoMapper photoDtoMapper){
        return new PhotoUseCaseImpl(photoService,photoDtoMapper);
    }
}
