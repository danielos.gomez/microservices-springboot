package com.service.photoservice.aplication.dto;

import lombok.Data;

import java.util.List;

@Data
public class PhotoDto {
    Long personId;
    List<String> photoList;
}
