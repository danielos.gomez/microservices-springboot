package com.service.photoservice.aplication.usescases;

import com.service.photoservice.aplication.dto.PhotoDto;

import java.util.List;

public interface PhotoUseCase {
    void savePhotos (PhotoDto photoDto);
    List<String> getPhotos (Long personId);
    void deletePhotos(Long personId);


}
