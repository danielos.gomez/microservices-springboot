package com.service.photoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class PhotoserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoserviceApplication.class, args);
	}

}
