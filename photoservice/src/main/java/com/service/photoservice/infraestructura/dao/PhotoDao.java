package com.service.photoservice.infraestructura.dao;


import com.service.photoservice.infraestructura.entity.PhotoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PhotoDao extends MongoRepository<PhotoEntity, String> {
PhotoEntity findPhotoEntityByPersonId(Long personId);
void deletePhotoEntityByPersonId(Long personId);
}
