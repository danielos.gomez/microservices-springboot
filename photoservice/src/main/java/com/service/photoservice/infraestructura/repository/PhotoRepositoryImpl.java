package com.service.photoservice.infraestructura.repository;

import com.service.photoservice.domain.model.Photo;
import com.service.photoservice.domain.ports.PhotoRepository;
import com.service.photoservice.infraestructura.dao.PhotoDao;
import com.service.photoservice.infraestructura.entity.PhotoEntity;
import com.service.photoservice.infraestructura.mapper.PhotoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoRepositoryImpl implements PhotoRepository {

    @Autowired
    PhotoDao photoDao;
    @Autowired
    PhotoMapper photoMapper;

    @Override
    public void savePhotos(Photo photo) {
        PhotoEntity photoEntity = photoMapper.toPhotoEntity(photo);
        photoDao.save(photoEntity);
    }

    @Override
    public List<String> getPhotosByPersonId(Long personId) {
        PhotoEntity photoEntity =photoDao.findPhotoEntityByPersonId(personId);
        List<String> listPhotos = photoEntity.getPhotoList();
        return  listPhotos;
    }

    @Override
    public void deletePhotosByPersonId(Long personId) {
        photoDao.deletePhotoEntityByPersonId(personId);
    }
}
