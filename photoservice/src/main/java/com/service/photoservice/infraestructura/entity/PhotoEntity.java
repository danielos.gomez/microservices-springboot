package com.service.photoservice.infraestructura.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.*;
import java.util.List;

@Data
@Document(collection = "imagen")
public class PhotoEntity {
    @Id
    @MongoId(FieldType.OBJECT_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    String photoId;
    Long personId;
    List<String> photoList;

}
