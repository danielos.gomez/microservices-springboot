package com.service.photoservice.infraestructura.mapper;

import com.service.photoservice.domain.model.Photo;
import com.service.photoservice.infraestructura.entity.PhotoEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PhotoMapper {
    PhotoEntity toPhotoEntity(Photo photo);
    List<PhotoEntity> toPhotoEntityList (List<Photo> photoList);

    @InheritInverseConfiguration
    Photo toPhoto(PhotoEntity photoEntity);
    List<Photo> toPhotoList  (List<PhotoEntity> photoEntityListList);

}
