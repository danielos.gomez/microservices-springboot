package com.service.photoservice.infraestructura.controllers;

import com.service.photoservice.aplication.dto.PhotoDto;
import com.service.photoservice.aplication.usescases.PhotoUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/photos")
public class PhotoController {
    @Autowired
    PhotoUseCase photoUseCase;

    @PostMapping
    public ResponseEntity insertPhotos(@RequestBody PhotoDto photoDto){
      try {
            photoUseCase.savePhotos(photoDto);
            return new ResponseEntity<>(HttpStatus.CREATED);
   } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{personId}")
    public ResponseEntity<List<String>> getPhotosByPersonId(@PathVariable("personId") Long personId ){
        return new ResponseEntity<List<String>>(photoUseCase.getPhotos(personId), HttpStatus.OK);
    }

    @DeleteMapping("/{personId}")
    public ResponseEntity deletePhotosByPersonId(@PathVariable("personId") Long personId ){
        try {
            photoUseCase.deletePhotos(personId);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }



}
