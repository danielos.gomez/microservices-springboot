package com.service.photoservice.domain.ports;

import com.service.photoservice.domain.model.Photo;

import java.util.List;

public interface PhotoRepository {

    void savePhotos(Photo photo);
    List<String> getPhotosByPersonId (Long personId);
    void deletePhotosByPersonId (Long personId);

}
