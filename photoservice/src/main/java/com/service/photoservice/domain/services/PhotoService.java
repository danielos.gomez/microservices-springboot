package com.service.photoservice.domain.services;

import com.service.photoservice.domain.model.Photo;

import java.util.List;

public interface PhotoService {
    void savePhoto(Photo photo);
    List<String> getPhotoListByPersonId( Long id);
    void deletePhotos(Long personId);
}
