package com.service.photoservice.domain.configuration;

import com.service.photoservice.domain.ports.PhotoRepository;
import com.service.photoservice.domain.services.PhotoService;
import com.service.photoservice.domain.servicesImpl.PhotoServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBeansDomain {

    @Bean
    public PhotoService photoServiceBean(PhotoRepository photoRepository){
        return  new PhotoServiceImpl(photoRepository);
    }
}
