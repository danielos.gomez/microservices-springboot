package com.service.photoservice.domain.servicesImpl;
import com.service.photoservice.domain.model.Photo;
import com.service.photoservice.domain.ports.PhotoRepository;
import com.service.photoservice.domain.services.PhotoService;
import java.util.List;

public class PhotoServiceImpl implements PhotoService {

    PhotoRepository photoRepository;

    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    public PhotoServiceImpl() {
    }

    @Override
    public void savePhoto(Photo photo) {
         photoRepository.savePhotos(photo);
    }

    @Override
    public List<String> getPhotoListByPersonId(Long id) {
        return photoRepository.getPhotosByPersonId(id);
    }

    @Override
    public void deletePhotos(Long personId) {
        photoRepository.deletePhotosByPersonId(personId);
    }
}
