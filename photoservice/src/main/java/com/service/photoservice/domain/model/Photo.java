package com.service.photoservice.domain.model;

import java.util.List;

public class Photo {
    String photoId;
    Long personId;
    List<String> photoList;

    public Photo(String photoId, Long personId, List<String> photoList) {
        this.photoId = photoId;
        this.personId = personId;
        this.photoList = photoList;
    }

    public Photo() {
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public List<String> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<String> photoList) {
        this.photoList = photoList;
    }
}
